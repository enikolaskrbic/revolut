package com.revolut.test.navigation

import com.revolut.test.HomeActivity
import com.revolut.test.R
import com.revolut.test.view.HomeFragment
import javax.inject.Inject

class NavigationController constructor(private val homeActivity: HomeActivity) {

    private val supportFragmentManager = homeActivity.supportFragmentManager
    private val homeContainer = R.id.fragment_placeholder

    fun navigateToHome(){
        supportFragmentManager
            .beginTransaction()
            .replace(homeContainer, HomeFragment())
            .commit()
    }
}