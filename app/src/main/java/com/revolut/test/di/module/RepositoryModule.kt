package com.revolut.test.di.module

import android.app.Application
import androidx.room.Room
import com.revolut.test.data.api.RevolutApi
import com.revolut.test.data.db.RevolutDatabase
import com.revolut.test.data.db.dao.RateDao
import com.revolut.test.data.repository.local.RateLocalDataSource
import com.revolut.test.data.repository.remote.RateRemoteDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * This class (module) provide all data sources, database, dao objects
 */
@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun bindRateRemoteDataSource(api: RevolutApi): RateRemoteDataSource = RateRemoteDataSource(api)

    @Singleton
    @Provides
    fun bindLocalRemoteDataSource(rateDao: RateDao): RateLocalDataSource = RateLocalDataSource(rateDao)

    @Singleton
    @Provides
    fun provideRevolutDatabase(application: Application): RevolutDatabase {
        return Room.databaseBuilder(application, RevolutDatabase::class.java, "revolut.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideRevolutrDao(revolutDatabase: RevolutDatabase): RateDao {
        return revolutDatabase.rateDao()
    }
}