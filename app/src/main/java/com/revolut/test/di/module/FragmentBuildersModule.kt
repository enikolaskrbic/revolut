package com.revolut.test.di.module

import com.revolut.test.view.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * This class (module) provide fragments
 *
 * Should add abstract function for every Fragment used in application
 */

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

}