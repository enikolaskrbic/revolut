package com.revolut.test.di.annotation
import javax.inject.Scope

/**
 * This annotation is used for activity scope in application
 */

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class Activity