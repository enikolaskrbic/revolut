package com.revolut.test.di.annotation

import javax.inject.Qualifier

/**
 * This annotation is used for local data sources
 */

@MustBeDocumented
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class Local