package com.revolut.test.di.module

import com.revolut.test.HomeActivity
import com.revolut.test.di.annotation.Activity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * This class (module) provide all activities in application
 */

@Suppress("unused")
@Module
abstract class ActivityBindingModule {

    @Activity
    @ContributesAndroidInjector(modules = [(FragmentBuildersModule::class)])
    abstract fun homeActivity(): HomeActivity
}