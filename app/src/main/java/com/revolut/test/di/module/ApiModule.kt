package com.revolut.test.di.module

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.revolut.test.BuildConfig
import com.revolut.test.data.api.RevolutApi
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * This class (module) provide retrofit classes for api calls
 */

@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    @Singleton
    fun provideHttpCatche(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }


    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }


    @Provides
    @Singleton
    fun providesApiOkHttpBuilder(cache: Cache): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.cache(cache)
        clientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        clientBuilder.readTimeout(30, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(30, TimeUnit.SECONDS)
        return clientBuilder.build()
    }

    @Singleton
    @Provides
    fun provideApiRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return getRetrofit(BuildConfig.BASE_URL, gson, okHttpClient)
    }

    @Provides
    @Singleton
    fun provideApiServices(retrofit: Retrofit): RevolutApi {
        return retrofit.create<RevolutApi>(RevolutApi::class.java)
    }


    private fun getRetrofit(baseUrl: String, gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()
    }
}