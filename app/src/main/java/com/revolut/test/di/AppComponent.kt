package com.revolut.test.di

import android.app.Application
import com.revolut.test.RevolutApp
import com.revolut.test.di.module.ActivityBindingModule
import com.revolut.test.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * This class generate all object needed for application
 */

@Singleton
@Component(modules = [
    (AppModule::class),
    (ActivityBindingModule::class),
    (AndroidInjectionModule::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent

    }

    fun inject(revolutApp: RevolutApp)
}