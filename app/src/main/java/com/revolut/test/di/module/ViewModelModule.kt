package com.revolut.test.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.revolut.test.di.annotation.ViewModelKey
import com.revolut.test.viewmodel.RateViewModel
import com.revolut.test.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * This class (module) provide view models
 *
 * Should add abstract function for every ViewModel used in application
 */

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(RateViewModel::class)
    abstract fun bindCurrencyViewModel(rateViewModel: RateViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
