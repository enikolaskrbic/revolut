package com.revolut.test.di.annotation

import javax.inject.Qualifier

/**
 * This annotation is used for remote data sources
 */

@MustBeDocumented
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class Remote