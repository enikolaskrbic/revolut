package com.revolut.test.util

import android.content.SharedPreferences

class SharedPreferenceManager constructor(private val sharedPreferences: SharedPreferences) {
    private val CURRENT_RATE = "CURRENT_RATE"

    var currentRate: String
        get() = sharedPreferences.getString(CURRENT_RATE, "EUR")?:""
        set(currentRate) = sharedPreferences.edit().putString(CURRENT_RATE, currentRate).apply()
}