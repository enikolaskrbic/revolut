package com.revolut.test.util

import android.content.Context
import androidx.core.content.ContextCompat
import com.revolut.test.R

object ImageUtil {

    fun getImage(context: Context?, rateId: String): Int{
        if(context == null) return R.drawable.ic_default
        return try {
            context.resources.getIdentifier("ic_${rateId.toLowerCase()}","drawable", context.packageName)
        }catch (e: Exception){
            R.drawable.ic_default
        }

    }
}