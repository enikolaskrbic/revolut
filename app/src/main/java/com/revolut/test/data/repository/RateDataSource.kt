package com.revolut.test.data.repository

import androidx.lifecycle.LiveData
import com.revolut.test.data.api.response.RatesResponse
import com.revolut.test.data.db.model.RateRoom
import com.revolut.test.data.error.ThrowableError
import com.revolut.test.data.state.ErrorState
import com.revolut.test.data.state.State
import com.revolut.test.data.state.SuccessState
import com.revolut.test.view.model.RateUI
import retrofit2.Response

interface RateDataSource {
    suspend fun save(rates: List<RateRoom>): State<List<Long>>
    suspend fun deselectCurrent()
    suspend fun setCurrent(rate: String)
    fun getRates(): LiveData<List<RateUI>>
    suspend fun getCurrent(): RateRoom?
    suspend fun getById(id: String): RateRoom?

    suspend fun fetchRates(base: String): Response<RatesResponse>
}