package com.revolut.test.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.revolut.test.data.db.model.RateRoom
import com.revolut.test.view.model.RateUI

@Dao
interface RateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(rates: List<RateRoom>): List<Long>

    @Query("SELECT * FROM rate WHERE id = :id")
    suspend fun getById(id: String): RateRoom?

    @Query("UPDATE rate SET isCurrent = 0")
    suspend fun deselectCurrent()

    @Query("UPDATE rate SET isCurrent = 1 WHERE id = :rate")
    suspend fun setCurrent(rate: String)

    @Query("SELECT * FROM rate order by displayOrder desc limit 1")
    suspend fun getCurrent(): RateRoom?

    @Query("SELECT * FROM rate order by displayOrder desc, id COLLATE NOCASE ASC")
    fun getRates(): LiveData<List<RateUI>>
}