package com.revolut.test.data.api.response

class RatesResponse(
    val rates: HashMap<String, Double> = hashMapOf(),
    val baseCurrency: String = "EUR"
)