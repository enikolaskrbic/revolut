package com.revolut.test.data.repository

import android.app.Application
import com.revolut.test.util.checkInternet
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkRepository @Inject constructor(private val application: Application) {

    fun isConnectedToNetwork() = application.checkInternet()
}