package com.revolut.test.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.revolut.test.data.db.dao.RateDao
import com.revolut.test.data.db.model.RateRoom

@Database(
    entities = [RateRoom::class], version = 1, exportSchema = false
)

@TypeConverters(Converters::class)
abstract class RevolutDatabase : RoomDatabase() {
    abstract fun rateDao(): RateDao

}