package com.revolut.test.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun listIntToJson(value: List<Int>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToListInt(value: String): List<Int> {
        val objects = Gson().fromJson(value, Array<Int>::class.java) as Array<Int>
        return objects.toList()
    }

    @TypeConverter
    fun listLongToJson(value: List<Long>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToListLong(value: String): List<Long> {
        val objects = Gson().fromJson(value, Array<Long>::class.java) as Array<Long>
        return objects.toList()
    }

    @TypeConverter
    fun listDoubleToJson(value: List<Double>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToListDouble(value: String): List<Double> {
        val objects = Gson().fromJson(value, Array<Double>::class.java) as Array<Double>
        return objects.toList()
    }


    @TypeConverter
    fun listToJson(value: List<String>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String): List<String> {
        val objects = Gson().fromJson(value, Array<String>::class.java) as Array<String>
        return objects.toList()
    }

}