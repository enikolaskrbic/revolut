package com.revolut.test.data.usecase

import com.revolut.test.data.db.model.RateRoom
import com.revolut.test.data.error.NoConnection
import com.revolut.test.data.error.UnknownError
import com.revolut.test.data.repository.RateRepository
import com.revolut.test.data.state.ErrorState
import com.revolut.test.data.state.State
import com.revolut.test.util.RateHelper
import com.revolut.test.util.SharedPreferenceManager
import kotlinx.coroutines.delay
import javax.inject.Inject

class FetchAndSaveRatesUseCase @Inject constructor(private val mRateRepository: RateRepository,
                                                   private val mSharedPreferenceManager: SharedPreferenceManager): UseCaseAsync<Unit, Unit>(){

    private val FETCH_TIME = 2000L

    override suspend fun invoke(value: Unit): State<Unit> {
        val response = try {
            mRateRepository.fetchRates(mSharedPreferenceManager.currentRate)
        }catch (e: Exception){
            return ErrorState(NoConnection)
        }
        if(!response.isSuccessful) return ErrorState(UnknownError)

        response.body()?.rates?.let { rates ->
            mRateRepository.save(rates.mapToRatesRoom(mSharedPreferenceManager.currentRate))
        }
        delay(FETCH_TIME)
        return invoke(value)
    }

    suspend fun HashMap<String, Double>.mapToRatesRoom(currentRate: String): List<RateRoom> {
        val returnList = mutableListOf<RateRoom>()
        for ((key, value) in this){
            val currentOrder = mRateRepository.getById(key)?.displayOrder?:0
            returnList.add(RateRoom(key, RateHelper.getNameById(key), value, currentOrder, currentRate == key))
        }
        return returnList
    }

}