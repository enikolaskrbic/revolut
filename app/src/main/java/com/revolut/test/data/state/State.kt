package com.revolut.test.data.state

import com.revolut.test.RevolutApp
import com.revolut.test.data.error.RevolutError


/**
 * This class is used to wrap data from data layer to presentation layer
 */

sealed class State<T>
data class SuccessState<T>(val data: T?, val isLoading : Boolean = false) : State<T>()
data class ErrorState<T>(val error: RevolutError): State<T>()

