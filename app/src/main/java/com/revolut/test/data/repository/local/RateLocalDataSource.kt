package com.revolut.test.data.repository.local

import androidx.lifecycle.LiveData
import com.revolut.test.data.db.dao.RateDao
import com.revolut.test.data.db.model.RateRoom
import com.revolut.test.data.error.ThrowableError
import com.revolut.test.data.state.ErrorState
import com.revolut.test.data.state.State
import com.revolut.test.data.state.SuccessState
import com.revolut.test.view.model.RateUI
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RateLocalDataSource @Inject constructor(private val mRateDao: RateDao) {

    suspend fun save(rates: List<RateRoom>): State<List<Long>> {
        return try {
            SuccessState(mRateDao.save(rates))
        }catch (t: Throwable){
            ErrorState(ThrowableError(t))
        }
    }

    suspend fun deselectCurrent() = mRateDao.deselectCurrent()

    suspend fun setCurrent(rate: String) = mRateDao.setCurrent(rate)

    fun getRates(): LiveData<List<RateUI>> = mRateDao.getRates()

    suspend fun getCurrent(): RateRoom? = mRateDao.getCurrent()

    suspend fun getById(id: String): RateRoom? = mRateDao.getById(id)
}