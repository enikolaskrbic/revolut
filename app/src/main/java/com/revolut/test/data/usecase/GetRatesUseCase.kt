package com.revolut.test.data.usecase

import androidx.lifecycle.LiveData
import com.revolut.test.data.repository.RateRepository
import com.revolut.test.view.model.RateUI
import javax.inject.Inject

class GetRatesUseCase @Inject constructor(private val mRateRepository: RateRepository): UseCase<Unit, LiveData<List<RateUI>>>(){
    override fun invoke(value: Unit): LiveData<List<RateUI>> {
        return mRateRepository.getRates()
    }

}