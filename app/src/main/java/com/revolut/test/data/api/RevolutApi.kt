package com.revolut.test.data.api

import com.revolut.test.data.api.response.RatesResponse
import retrofit2.Response
import retrofit2.http.*

interface RevolutApi {

    @GET("latest")
    suspend fun fetchRates(@Query("base") base: String): Response<RatesResponse>

}