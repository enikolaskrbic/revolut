package com.revolut.test.data.usecase

import com.revolut.test.data.error.NoConnection
import com.revolut.test.data.error.UnknownError
import com.revolut.test.data.repository.NetworkRepository
import com.revolut.test.data.repository.RateRepository
import com.revolut.test.data.state.ErrorState
import com.revolut.test.data.state.State
import com.revolut.test.data.state.SuccessState
import com.revolut.test.util.SharedPreferenceManager
import javax.inject.Inject

class SetCurrentRateUseCase @Inject constructor(private val mRateRepository: RateRepository,
                                                private val mNetworkRepository: NetworkRepository,
                                                private val mSharedPreferenceManager: SharedPreferenceManager): UseCaseAsync<String, Unit>(){
    override suspend fun invoke(value: String): State<Unit> {
        if(!mNetworkRepository.isConnectedToNetwork()) return ErrorState(NoConnection)
        mRateRepository.deselectCurrent()
        mSharedPreferenceManager.currentRate = value
        val displayOrder = mRateRepository.getCurrent()?.displayOrder?:1
        val rate = mRateRepository.getById(value)?:return ErrorState(UnknownError)
        rate.displayOrder = displayOrder + 1
        mRateRepository.save(listOf(rate))
        return SuccessState(Unit)
    }

}