package com.revolut.test.data.usecase

import com.revolut.test.data.state.State

abstract class UseCase<in Value, Type> {

    abstract fun invoke(value: Value): Type
}

abstract class UseCaseAsync<in Value, Type> {

    abstract suspend fun invoke(value: Value): State<Type>
}