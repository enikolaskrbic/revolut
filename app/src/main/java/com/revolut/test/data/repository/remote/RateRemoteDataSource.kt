package com.revolut.test.data.repository.remote

import com.revolut.test.data.api.RevolutApi
import com.revolut.test.data.api.response.RatesResponse
import retrofit2.Response
import retrofit2.http.Query
import javax.inject.Inject

class RateRemoteDataSource @Inject constructor(private val mRevolutApi: RevolutApi) {

    suspend fun fetchRates(base: String): Response<RatesResponse> = mRevolutApi.fetchRates(base)
}