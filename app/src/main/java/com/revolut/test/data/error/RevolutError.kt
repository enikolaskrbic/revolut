package com.revolut.test.data.error

sealed class RevolutError
object NoConnection: RevolutError()
object UnknownError: RevolutError()
data class BackendError(val message: String): RevolutError()
data class ExceptionError(val exception: Exception) : RevolutError()
data class ThrowableError(val throwable: Throwable) : RevolutError()