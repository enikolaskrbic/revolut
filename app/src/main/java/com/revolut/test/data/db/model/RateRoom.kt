package com.revolut.test.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rate")
data class RateRoom(
    @PrimaryKey
    var id: String,
    var name: String,
    var value: Double,
    var displayOrder: Long,
    var isCurrent: Boolean)