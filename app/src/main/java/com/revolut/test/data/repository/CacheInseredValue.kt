package com.revolut.test.data.repository

object CacheInseredValue {

    var insertedValue = 100.0

    fun onChangeValue(value: Double){
        insertedValue = value
    }

    fun clear(){
        insertedValue = 100.0
    }
}