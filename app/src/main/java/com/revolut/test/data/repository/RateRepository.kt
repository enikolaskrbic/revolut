package com.revolut.test.data.repository

import androidx.lifecycle.LiveData
import com.revolut.test.data.api.response.RatesResponse
import com.revolut.test.data.db.model.RateRoom
import com.revolut.test.data.repository.local.RateLocalDataSource
import com.revolut.test.data.repository.remote.RateRemoteDataSource
import com.revolut.test.data.state.State
import com.revolut.test.view.model.RateUI
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RateRepository @Inject constructor(private val mLocalDataSource: RateLocalDataSource, private val mRemoteDataSource: RateRemoteDataSource): RateDataSource {
    override suspend fun save(rates: List<RateRoom>): State<List<Long>> {
        return mLocalDataSource.save(rates)
    }

    override suspend fun deselectCurrent() {
        mLocalDataSource.deselectCurrent()
    }

    override suspend fun setCurrent(rate: String) {
        mLocalDataSource.setCurrent(rate)
    }

    override fun getRates(): LiveData<List<RateUI>> {
        return mLocalDataSource.getRates()
    }

    override suspend fun getCurrent(): RateRoom? {
        return mLocalDataSource.getCurrent()
    }

    override suspend fun getById(id: String): RateRoom? {
        return mLocalDataSource.getById(id)
    }

    override suspend fun fetchRates(base: String): Response<RatesResponse> {
        return mRemoteDataSource.fetchRates(base)
    }



}