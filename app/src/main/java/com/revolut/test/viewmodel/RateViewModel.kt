package com.revolut.test.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.revolut.test.data.state.ErrorState
import com.revolut.test.data.state.State
import com.revolut.test.data.state.SuccessState
import com.revolut.test.data.usecase.FetchAndSaveRatesUseCase
import com.revolut.test.data.usecase.GetRatesUseCase
import com.revolut.test.data.usecase.SetCurrentRateUseCase
import com.revolut.test.view.model.RateUI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.launch
import javax.inject.Inject

class RateViewModel @Inject constructor(private val mFetchAndSaveRatesUseCase: FetchAndSaveRatesUseCase,
                                        private val mSetCurrentRateUseCase: SetCurrentRateUseCase,
                                        private val mGetRatesUseCase: GetRatesUseCase) : ViewModel() {

    val getRates = mGetRatesUseCase.invoke(Unit)
    val setCurrentLiveData = MutableLiveData<State<Pair<RateUI, String>>>()
    private var fetchJob: Job? = null

    fun fetchRates(){
        fetchJob = viewModelScope.launch(Dispatchers.IO) {
            mFetchAndSaveRatesUseCase.invoke(Unit)
        }
    }

    fun setCurrent(rate: RateUI, currentValue: String){
        viewModelScope.launch(Dispatchers.IO) {
            fetchJob?.cancelAndJoin()
            when(val response = mSetCurrentRateUseCase.invoke(rate.id)){
                is SuccessState ->{
                    setCurrentLiveData.postValue(SuccessState(Pair(rate, currentValue)))
                    fetchRates()
                }
                is ErrorState -> setCurrentLiveData.postValue(ErrorState(response.error))
            }

        }
    }

}