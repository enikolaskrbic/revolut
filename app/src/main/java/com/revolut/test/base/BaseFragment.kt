package com.revolut.test.base

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.revolut.test.HomeActivity
import com.revolut.test.R
import com.revolut.test.data.error.NoConnection
import com.revolut.test.data.error.RevolutError
import com.revolut.test.data.error.UnknownError
import com.revolut.test.di.Injectable
import com.revolut.test.navigation.NavigationController
import javax.inject.Inject

open class BaseFragment: Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mNavigationController : NavigationController

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mNavigationController = (activity as HomeActivity).getNavigationController()
    }

    fun showToast(message: String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun handleError(error: RevolutError){
        when(error){
            is UnknownError -> {
                showToast(getString(R.string.error_unknown))
            }
            is NoConnection -> {
                showToast(getString(R.string.check_network))
            }
        }
    }
}