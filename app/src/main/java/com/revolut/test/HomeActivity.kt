package com.revolut.test

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.revolut.test.base.BaseActivity
import com.revolut.test.navigation.NavigationController

class HomeActivity : BaseActivity() {

    private lateinit var mNavigationController: NavigationController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        window.statusBarColor = ContextCompat.getColor(this, R.color.status_color)
        mNavigationController = NavigationController(this)
        mNavigationController.navigateToHome()
    }

    fun getNavigationController() = mNavigationController
}
