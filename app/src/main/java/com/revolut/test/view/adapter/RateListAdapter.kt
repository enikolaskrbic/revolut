package com.revolut.test.view.adapter

import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.revolut.test.R
import com.revolut.test.data.repository.CacheInseredValue
import com.revolut.test.util.*
import com.revolut.test.view.model.RateUI
import kotlinx.android.synthetic.main.row_currency.view.*

class RateListAdapter(private val event: (RateEvent) -> Unit): ListAdapter<RateUI, RateListAdapter.RateViewHolder>(
    DIFF_CALLBACK){

    private var mRecyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    fun setData(list: List<RateUI>){
        submitList(list)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<RateUI>() {
            override fun areItemsTheSame(oldItem: RateUI, newItem: RateUI): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: RateUI, newItem: RateUI): Boolean {
                return oldItem.isCurrent == newItem.isCurrent && oldItem.value == newItem.value
            }
        }
    }

    inner class RateViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bind(rate: RateUI, position: Int){
            with(itemView){
                image_view_rate.setImageResource(ImageUtil.getImage(context, rate.id))
                text_view_rate_title.text = rate.id
                text_view_rate_description.text = rate.name
                view_over_item.setOnClickListener {
                    event.invoke(RateEvent.OnSelectedRate(rate, edit_text_value.text.toString()))
                }
                edit_text_value.setText((rate.value * CacheInseredValue.insertedValue).formatNumber())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder {
        return RateViewHolder(inflate(R.layout.row_currency, parent))
    }

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }
}


