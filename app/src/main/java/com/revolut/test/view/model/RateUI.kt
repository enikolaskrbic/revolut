package com.revolut.test.view.model

data class RateUI(val id: String,
                  val name: String,
                  val value: Double,
                  val isCurrent: Boolean)