package com.revolut.test.view.adapter

import com.revolut.test.view.model.RateUI

sealed class RateEvent {
    data class OnSelectedRate(val rate: RateUI,val currentInsertData: String): RateEvent()
}