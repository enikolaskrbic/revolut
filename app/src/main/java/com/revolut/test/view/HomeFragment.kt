package com.revolut.test.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import com.revolut.test.R
import com.revolut.test.base.BaseFragment
import com.revolut.test.data.repository.CacheInseredValue
import com.revolut.test.data.state.ErrorState
import com.revolut.test.data.state.SuccessState
import com.revolut.test.util.*
import com.revolut.test.view.adapter.RateEvent
import com.revolut.test.view.adapter.RateListAdapter
import com.revolut.test.view.model.RateUI
import com.revolut.test.viewmodel.RateViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.row_currency.*
import kotlinx.android.synthetic.main.row_currency.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class HomeFragment : BaseFragment(){

    lateinit var mRateViewModel: RateViewModel
    var mRateListAdapter: RateListAdapter? = null
    var isInit = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mRateViewModel = viewModel(viewModelFactory)
        mRateViewModel.fetchRates()
        view_over_item.setVisibleOrGone(false)
        initObservers()
        initRecyclerView()
        initListeners()

    }

    private fun initListeners() {
        edit_text_value.onTextChange {
            CacheInseredValue.insertedValue = it.getDecimalNumber()
            recycler_view_rates?.post {
                mRateListAdapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        CacheInseredValue.insertedValue = 100.0
    }

    private fun initRecyclerView() {
        if(mRateListAdapter == null){
            mRateListAdapter = RateListAdapter {
                when(it){
                    is RateEvent.OnSelectedRate -> {
                        mRateViewModel.setCurrent(it.rate, it.currentInsertData)
                    }
                }
            }
        }
        if(recycler_view_rates.adapter == null){
            recycler_view_rates.adapter = mRateListAdapter
            recycler_view_rates.itemAnimator = DefaultItemAnimator()
        }
    }

    private fun initCurrent(rate: RateUI, currentValue: String, isVisible: Boolean){
        selected_row.setVisibleOrInvisible(isVisible)
        edit_text_value?.setText(currentValue)
        if(!isVisible)
            edit_text_value?.requestFocus()
        edit_text_value?.setSelection(edit_text_value.text.toString().length)
        image_view_rate?.setImageResource(ImageUtil.getImage(context, rate.id))
        text_view_rate_title?.text = rate.id
        text_view_rate_description.text = rate.name
    }

    private fun initObservers() {
        mRateViewModel.getRates.observe(viewLifecycleOwner, Observer {
            if(it.isEmpty()) return@Observer
            if(!isInit){
                isInit = true
                initCurrent(it[0], CacheInseredValue.insertedValue.formatNumber(), true)
            }
            mRateListAdapter?.setData(it)

        })

        mRateViewModel.setCurrentLiveData.observe(viewLifecycleOwner, Observer {
            when(it){
                is SuccessState -> {
                    it.data?.let {
                        initCurrent(it.first, it.second, false)
                        GlobalScope.launch(Dispatchers.Main) {
                            delay(500)
                            selected_row?.setVisibleOrInvisible(true)
                        }
                    }
                }
                is ErrorState -> handleError(it.error)
            }

        })

    }


}